Nous avons fait une simple application java avec une intreface toute simple (JFrame Form)
Executer l’appli via docker
 - Commande faite :
•	Creation d’un dossier = mkdir Monapp +
•	 Creation du dockerfile avec les parametres suivants : 
       FROM openjdk:18
COPY . /C:/Users/usr/devop
WORKDIR C:/Users/usr/devop
RUN javac cuisine.java
CMD ["java", "cuisine"]  
•	Puis on va  compiler et exécuter notre projet via notre dockerfile avce la command e :  docker build . -f DockerFile.txt -t devop2
•	Devop2 est mon image 
•	Ensuite on va chercher notre id de l image et on run l’appli :
 - docker images
REPOSITORY    TAG            IMAGE ID       CREATED              SIZE
devop2        latest         c439d56b0fa5   About a minute ago   475MB
devop         latest         10b526116e2d   About an hour ago    312MB
mysqlapp      latest         705a43d43f75   2 hours ago          305MB
mywebapp      latest         705a43d43f75   2 hours ago          305MB
openjdk       18             fd3416e1e207   3 weeks ago          464MB
nginx         alpine         d7c7c5df4c3a   2 months ago         23.4MB
alpine/git    latest         b337a04161f7   2 months ago         38.2MB
php           latest         8b13149eddaa   2 months ago         484MB
ubuntu        latest         ff0fea8310f3   2 months ago         72.8MB
php           8.1.3-apache   4319409d3dfb   3 months ago         458MB
hello-world   latest         feb5d9fea6a5   8 months ago         13.3kB

- docker run -d -p 8086:80 c439d56b0fa5


